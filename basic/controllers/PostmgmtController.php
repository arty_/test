<?php

namespace   app\controllers;
use yii;

class PostmgmtController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionCreate()
    {
        $model= new \app\models\Post();
        if($model->load(yii::$app->request->Post()))
        {
        	$model->post_name=strtoupper($model->post_name);
        	if($model->salary<5000)
        		$model->salary=5000;
        	// $model->save();
        	$model->status=1;
        	if(!$model->save())
        		var_dump($model->errors);
        	die;
        	return $this->redirect(['create']);
         // print_r($model);
          // die;
        }
        else
        {
        	return $this->render('create',['model'=>$model]);
        }
    }

}
