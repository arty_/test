<?php

namespace app\controllers;
use yii;
class SignupController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	$model = new \app\models\Utable();
    	if($model->load(yii::$app->request->post()))
    	{
         $model->save();
         return $this->redirect(['site/index']);
        
    	}else
    	{
        return $this->render('index',['model'=>$model]);
        }
    }

}
