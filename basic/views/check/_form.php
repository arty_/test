<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$data=\yii\helpers\ArrayHelper::map(\app\models\post::find()->all(),'id','post_name');
/* @var $this yii\web\View */
/* @var $model app\models\Check */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true,'class'=>'w3_input']) ?>

    <?= $form->field($model, 'post')->dropDownList($data,['prompt'=>'select']); ?>

    <?= $form->field($model, 'joined_date')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
