<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <nav  class="navbar  navbar-default">
    <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">yii</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <?php if(!yii::$app->user->isGuest){?><li ><a href="#">Home</a></li><?php }?>
     <?php if(!yii::$app->user->isGuest){?> <li> <a href="<?=Yii::$app->homeUrl ?>/?r=postmgmt/create"> Post </a> </li><?php }?>
      <?php if(!yii::$app->user->isGuest){?><li> <a href="<?=Yii::$app->homeUrl ?>/?r=checkmgmt/create"> check </a> </li><?php }?>
      <li class="dropdown">
<?php if(!yii::$app->user->isGuest){?><a class="dropdown-toggle"  data-toggle="dropdown" href="#"> signup <span class="caret"> </span></a><?php }?>
        <ul class="dropdown-menu">
            <li> <a href="#">create </a></li>
            <li><a href="#"> list </a> </li>
            <li> <a href="#"> update </a> </li>
            
        </ul>
        </li>
       <?php if(yii::$app->user->isGuest){?><li><a href="<?=Yii::$app->homeUrl?>/?r=signup/index">Signup</a></li><?php }?>
      <?php if(yii::$app->user->isGuest){ ?><li><a href="<?=Yii::$app->homeUrl ?>?r=/site/login" data-method="post">login</a></li><?php } ?>
      <?php if(!yii::$app->user->isGuest){ ?><li><a href="<?=Yii::$app->homeUrl ?>?r=/site/logout" data-method="post">Logout</a></li><?php } ?>
    </ul>
  </div>

    </nav>  
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
