<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\utable */

$this->title = 'Create Utable';
$this->params['breadcrumbs'][] = ['label' => 'Utables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
