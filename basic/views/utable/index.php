<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Utable */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Utables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Utable', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'password',
            'authkey',
            'aceessToken',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
