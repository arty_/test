<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check".
 *
 * @property integer $id
 * @property string $name
 * @property integer $post
 * @property integer $joined_date
 * @property integer $status
 *
 * @property Post $post0
 */
class Check extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'post', 'joined_date', 'status'], 'required'],
            [['post', 'joined_date', 'status'], 'integer'],
            [['name'], 'string', 'max' => 30],
            [['post'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'post' => 'Post',
            'joined_date' => 'Joined Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost0()
    {
        return $this->hasOne(Post::className(), ['id' => 'post']);
    }
}
