<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utable".
 *
 * @property integer $id
 * @property integer $username
 * @property integer $password
 * @property integer $authkey
 * @property integer $aceessToken
 */
class Utable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'utable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password', 'authkey', 'aceessToken'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'authkey' => 'Authkey',
            'aceessToken' => 'Aceess Token',
        ];
    }
}
